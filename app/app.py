# -*- coding: utf-8 -*-
"""
Counteractual
"""
# flask imports
from flask import Flask
from flask import request
from flask import render_template
from flask import send_file
from flask import make_response
from flask import url_for
from flask_assets import Bundle, Environment
from flask_cors import CORS
import tensorflow as tf
from tensorflow import keras
from sklearn.preprocessing import StandardScaler
from scipy.special import boxcox1p
import dice_ml
from dice_ml.utils import helpers
import json
import ast
import pickle
from io import StringIO, BytesIO
import os
import pandas as pd
import numpy as np
import plotly
from plotly import graph_objs as go
import warnings
warnings.filterwarnings('ignore')

app = Flask(__name__)

js = Bundle('js/scripts.js', output='js/main.js')
assets = Environment(app)
assets.register('main_js', js)


@app.route('/cf_method')
def counterfact_page():
    keys = 'static/keys.sav'
    plot_keys = pickle.load(open(keys, "rb"))
    return render_template('cf_mat1.html',
                           plot_keys=plot_keys,)


@app.route('/cf_method_out', methods=['GET', 'POST'])
def counterfact_page2():

    # text insert
    query_instance = request.form['textinput']
    query_instance = ast.literal_eval(query_instance)

    # load json meta-data
    with open('static/feature_space.json') as read_file:
        feature_space = json.load(read_file)

    # Option A: load model
    model_version = "001"
    model_name = "stab_classifier"
    model_path = os.path.join(model_name, model_version)
    model = tf.keras.models.load_model(model_path)

    # Oprion B: load model function from Tensorflow Serving / REST Request
    # payload = json.dumps({
    #    "signature_name":"serving_default",
    #    "instances": single.tolist(),
    #    })
    #SERVER_URL = "http://localhost ..."
    #model = model_function = lambda x: np.array(requests.post(SERVER_URL, data = x).json()["predictions"])

    # DiCE
    num_cf = 3
    backend = 'TF'+tf.__version__[0]  # TF1
    m = dice_ml.Model(model=model, backend=backend)

    # Option A: Metadata Instance
    d_meta = dice_ml.Data(features=feature_space,
                          outcome_name='decomposition_energy')

    # Option B: Dataset Access Instance
    # d_train = dice_ml.Data(dataframe=train, continuous_features=[], outcome_name='decomposition_energy')

    exp = dice_ml.Dice(d_meta, m)
    dice_export = exp.generate_counterfactuals(
        query_instance, total_CFs=num_cf, desired_class="opposite")

    # DiCE documentation modification
    dice_export_df = dice_export

    test_instance_updated = pd.DataFrame(np.array([np.append(dice_export_df.test_instance, dice_export_df.test_pred)]),
                                         columns=dice_export_df.data_interface.encoded_feature_names+[dice_export_df.data_interface.outcome_name])

    org_instance = dice_export_df.data_interface.from_dummies(
        test_instance_updated)
    org_instance = org_instance[dice_export_df.data_interface.feature_names + [
        dice_export_df.data_interface.outcome_name]]
    dice_export_df.org_instance = dice_export_df.data_interface.de_normalize_data(
        org_instance)

    cfs = np.array([dice_export_df.final_cfs[i][0]
                    for i in range(len(dice_export_df.final_cfs))])

    result = dice_export_df.data_interface.get_decoded_data(cfs)
    result = dice_export_df.data_interface.de_normalize_data(result)

    v = dice_export_df.data_interface.get_decimal_precisions()
    for ix, feature in enumerate(dice_export_df.data_interface.continuous_feature_names):
        result[feature] = result[feature].astype(float).round(v[ix])

    # predictions for CFs
    test_preds = [np.round(preds.flatten().tolist(), 3)
                  for preds in dice_export_df.final_cfs_preds]
    test_preds = [item for sublist in test_preds for item in sublist]
    test_preds = np.array(test_preds)

    result[dice_export_df.data_interface.outcome_name] = test_preds
    result = result.reindex(columns=org_instance.columns)
    dice_export_df.final_cfs_df = result[dice_export_df.data_interface.feature_names + [
        dice_export_df.data_interface.outcome_name]]
    dice_export_df.final_cfs_list = dice_export_df.final_cfs_df.values.tolist()

    # CF table build
    cf_table = pd.concat(
        [org_instance, result])\
        .reset_index(drop=True)\
        .rename(index={0: 'Query Instance'})
    cf_table = cf_table.drop(columns=cf_table.columns[-1])

    # figure 1: counterfactual table
    counterfact_table = go.Figure(data=[go.Table(
        header=dict(values=list(cf_table.reset_index().columns),
                    font=dict(color='black', size=12),
                    line_color='darkslategray'),
        cells=dict(values=cf_table.reset_index().T.
                   replace(np.nan, '', regex=True).values.tolist(),
                   font=dict(size=14)
                   ))])

    counterfact_table.update_layout(
        title="Counterfactual Explanations (set of %s)<br>Feature Comparison to Initial Query" %
        (num_cf),
        height=300,
        width=1850,
    )

    # json
    counterfact_plot = json.dumps(counterfact_table, cls=plotly.utils.PlotlyJSONEncoder)

    # load model history plots
    history = 'static/history_plot.sav'
    classifier = 'static/classifier_plot.sav'
    history_plot = pickle.load(open(history, "rb"))
    metrics_plot = pickle.load(open(classifier, "rb"))

    return render_template('cf_mat2.html',
                           plot_B=counterfact_table,
                           plot_C=history_plot,)


if __name__ == "__main__":
    app.run(debug=True, port=5014,
            host='0.0.0.0',
            )