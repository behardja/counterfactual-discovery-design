## Counterfactuals for Discovery and Design - Web App

This web application is a counterpart to my Towards Data Science blog post, [“Counterfactual Explanations in Model Interpretations.”](https://towardsdatascience.com/counterfactual-explanations-in-model-interpretations-a73caec5b74b) The value behind this exercise is to extract model insights and explanations to produce specific actions that can accelerate discoveries and inform solutions.  
 
This app uses a trained model and takes in input properties to output which specific feature changes are needed in order to reach a desired outcome. This particular example is based on an analysis of materials properties data used to build a deep learning model to predict product stability.

### Getting Started

To bring up the web application locally, clone, build, and run the following using docker. The app will run on port ‘5014’ 
```
git clone https://gitlab.com/behardja/counterfactual-discovery-design.git
```
```
docker run -p 5014:5014 --rm -it $(docker build -q .)
```
### How it works
The input field takes in a JSON key query of materials properties. This is then processed through a trained classifier model and into counterfactuals, where new instances are returned. These consist of newly changed features that lead to a desired target response (ie change the crystal structure to tetrahedral and the chemical composition to include aluminum to synthesize materials of improved stability).

#### Demo
![input_screen](app/static/demo.gif)

#### Input Keys

| Feature                     | Datatype | Description                                                      |
|-----------------------------|----------|------------------------------------------------------------------|
| bandgap_orig                | float64  | Energy band gap of this material                                 |
| energy_vdw_per_atom_orig    | float64  | Energy with vdW correction (eV/atom)                             |
| total_magnetization         | float64  | Total magnetic moment in μB                                      |
| exfoliation_energy_per_atom | float64  | Energy required to cleave off layer of the 2D material (eV/atom) |
| is_gap_direct               | boolean  | Is the material a direct gap                                     |
| is_metal                    | boolean  | Is the material metallic                                         |
| crystal_system              | string   | Relaxed crystal structure represented in dictionary              |
| elements                    | string   | Elemental composition of material                                |

### Additional Reference
* Mothilal, R. K., Sharma A., and Tan, C. (2020) “Explaining machine learning classifiers through diverse counterfactual explanations.”
* Zhou, J., Shen, L., Costa, M.D. et al. (2019) "2DMatPedia, an open computational database of two-dimensional materials from top-down and bottom-up approaches." Sci Data 6, 86


### Bring Your Own Model 
(coming soon)

<!-- ![input_screen](app/static/screen1.png) -->