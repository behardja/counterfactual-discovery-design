# pull official base image
FROM python:3.7-slim

# install system dependencies
RUN apt-get clean && apt-get update -y

# copy contents
COPY /app /app

# install system dependencies
RUN pip install --upgrade pip
RUN pip install --no-cache-dir -r ./app/requirements.txt

# set work directory
WORKDIR /app

# set port
EXPOSE 5014

# command
CMD ["gunicorn", "-t", "1000", "-w", "4", "-b", "0.0.0.0:5014", "wsgi:app", "--preload"]